interface Tech {
  id?: number;
  name?: string;
  width?: string;
  height?: string;
  color?: string;
  level?: string;
  image: string;
}

export default Tech;