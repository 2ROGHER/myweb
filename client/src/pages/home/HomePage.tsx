import NavContainer from "../../components/container/navcontainer/NavContainer";
import PresentationContainer from "../../components/container/presentation/PresentationContainer";
import ProjectContainer from "../../components/container/projectscontainer/ProjectContainer";
import TechStackContainer from "../../components/pure/tech/techstack/TechStack";

export const HomePage = () => {
  return (
    <>
      <NavContainer />

      <PresentationContainer />

      {/* Tech stack parth in home page  */}
      <TechStackContainer />

      {/* Projects list */}
      <ProjectContainer />
    </>
  );
};
