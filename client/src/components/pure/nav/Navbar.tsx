import logo from '../../../assets/svg/logo.svg';
import './Navbar.style.css';

const Navbar = () => {
  return (
    <nav className='menu'>
      <ul>
        <li id='logo'><img src={logo} alt='logo' style={{width: '36px'}}></img></li>
        <li>home</li>
        <li>stack</li>
        <li>about</li>
        <li>projects</li>
      </ul>
    </nav>
  );
};
export default Navbar;
