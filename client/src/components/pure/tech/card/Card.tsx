import Tech from "../../../../models/interfaces/Tech.interface";
import "./card.style.css";

const Card = (data: Tech): JSX.Element => {
  return (
    <div className="img-container">
      <img src={data.image} style={{ width: "130px" }} alt="tech-stack" />
    </div>
  );
};

export default Card;
