import "./techstack.style.css";
import TechList from "../techlist/TechList";
import { useEffect } from "react";

const TechStack = (): JSX.Element => {
  return (
    <main className="tech">
      <header>
        <h1>tech stack</h1>
        <h3>Technologies for Full-Stack Developer</h3>
      </header>
      <section className="slider">
        <div className="slider-track-l">
          <TechList />
          <TechList />
          <TechList />
        </div>
      </section>
      <section className="slider">
        <div className="slider-track-r">
          <TechList />
          <TechList />
          <TechList />
        </div>
      </section>
    </main>
  );
};

export default TechStack;
