import Card from "../card/Card";
import data from "../../../../data/tech.data.json";
import Tech from "../../../../models/interfaces/Tech.interface";

export default function TechList() {
  return (
    <>
      {data?.map((t: Tech): any => {        
        return (
          <Card
            id={t.id}
            name={t.name}
            image={t.image}
            color={t.color}
            level={t.level}
            width={t.width}
            height={t.height}
          />
        );
      })}
    </>
  ); // this componente returns a list of cards.
}
