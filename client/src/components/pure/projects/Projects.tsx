import Frame from '../frame/Frame';
import './projects.style.css';

const Projects = () => {
  return (
    <main className="projects">
      <header>
        <h1>awesome projects</h1>
        <h3>things I’ve built </h3>
      </header>
      <section className='projects-container'>
        <Frame h={100} w={100}/>
      </section>
    </main>
  );
};
export default Projects;
