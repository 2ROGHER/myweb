import React from "react";
import "./frame.style.css";

interface FrameProps {
  h: number;
  w: number;
}


const Frame: React.FC<FrameProps> = ({ h, w }) => {
  return (
    <div className="frame" style={{ height: h + "%", width: w + "%" }}></div>
  );
};

export default Frame;

