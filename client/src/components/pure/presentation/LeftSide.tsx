import React, { useState } from "react";
import "./leftside.styles.css";
import spell from "../../../services/spelling";
/**
 * This function returns a React Component to render the left side of
 * the presentation.
 * @returns React Component
 */
let data = [
  "Student",
  "Artist",
  "Programmer",
  "Developer",
  "Software Engineer",
];
let colors = ["#fce87b", "#e5c9f3", "#ffa6c5", "#9af6d3", "#62d9fa"];

// "Artist",
//   "Programmer",
//   "Software Developer",
function* generator() {
  var l: String = "";
  for (let i = 0; i <= data.length; i++) {
    for (let j = 0; j <= data[i].length - 1; j++) {
      yield (l += data[i][j]);
    }
    for (let k = l.length - 1; k >= 0; k--) {
      yield l.slice(0, k);
    }
    l = "";
  }
}
const LeftSide = (): any => {
  const [l, setL] = useState<String>("");
  React.useEffect((): any => {
    const g = generator();
    const updateL = () => {
      const { value, done } = g.next();
      if (!done) {
        setL(value);
        setTimeout(updateL, 100);
      }
    };
    updateL();
  }, []);

  return (
    <div>
      <div className="row-1">
        <h1 className="row-1">
          I am{" "}
          <span
            style={{ color: `${colors[Math.floor(Math.random() * (4 - 1))]}` }}
          >
            {l}
          </span>
        </h1>
      </div>

      <div className="row-2">
        <h3>
          I can <strong style={{ color: "#fce87b" }}>build</strong> anything you
          can <strong style={{ color: "#9af6d3" }}>dreams.</strong>
        </h3>
      </div>
    </div>
  );
};

export default LeftSide;
