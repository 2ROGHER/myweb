import LeftSide from "../../pure/presentation/LeftSide";
import RightSide from "../../pure/presentation/RightSide";
import './presentation.styles.css';

const PresentationContainer = () => {
  return (
    <section className="pres-main">
      <div className="col-1">
        <LeftSide />
      </div>
      <div className="col-2">
        <RightSide />
      </div>
    </section>
  );
};

export default PresentationContainer;